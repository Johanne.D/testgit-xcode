//
//  ContentView.swift
//  Shared
//
//  Created by Johanne DAUPHIN on 30/11/2021.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        VStack{
            Text("Hello, world!")
                .padding()
            Text("Essai de Commit")
            Text("Valider")
        }
       
        
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
