//
//  TestGitApp.swift
//  Shared
//
//  Created by Johanne DAUPHIN on 30/11/2021.
//

import SwiftUI

@main
struct TestGitApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
